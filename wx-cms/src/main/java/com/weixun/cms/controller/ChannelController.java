package com.weixun.cms.controller;

import com.jfinal.core.Controller;
import com.jfinal.json.JFinalJson;
import com.weixun.cms.model.vo.Channel;
import com.weixun.cms.service.ChannelService;
import com.weixun.comm.model.vo.Ztree;
import com.weixun.model.CmsArticle;
import com.weixun.model.CmsChannel;
import com.weixun.utils.ajax.AjaxMsg;

import java.util.ArrayList;
import java.util.List;

public class ChannelController extends Controller {

    static ChannelService channelService = new ChannelService();


    /**
     * 删除数据
     */
    public void delete()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        String channel_pk = this.getPara("channel_pk");
        int res =channelService.deleteById(channel_pk);
        if (res >0) {
            ajaxMsg.setState("success");
            ajaxMsg.setMsg("删除成功");
        }
        else
        {
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("删除失败");
        }

        renderJson(ajaxMsg);
    }

    /**
     * 保存方法
     */
    public void saveOrUpdate()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            CmsChannel channel = getModel(CmsChannel.class,"");
            if (channel.getChannelPk() != null && !channel.getChannelPk().equals(""))
            {
                //更新方法
                res = channel.update();
            }
            else {
                //保存方法
                res = channel.save();
            }
            if(res)
            {
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("保存成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("保存失败");
            }
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("保存失败");
        }
        renderJson(ajaxMsg);
    }

    /**
     * 查询数据列表
     */
    public void list()
    {
        CmsChannel cmsChannel = getModel(CmsChannel.class,"");
        List<CmsChannel> cmsChannels= channelService.findList(cmsChannel);
        renderJson(JFinalJson.getJson().toJson(cmsChannels));

    }


    /**
     * 获取treegrid数据
     */
    public void channellist(){

        Channel Channel = new Channel();
        Channel.setId(0);
        List<Channel> channels = this.tree(Channel);
        renderJson(JFinalJson.getJson().toJson(channels));
    }

    /**
     * 使用递归查询树形菜单
     * @param menu
     * @return
     */
    List<Channel> root = new ArrayList<Channel>();
    private List<Channel> tree(Channel channel)
    {

        Channel tmp_channel;
        List<Channel> channel_childs = new ArrayList<Channel>();
        List<CmsChannel> channel_parents= channelService.findChildList(channel);
        if (channel_parents != null && channel_parents.size()>0)
        {
            for (CmsChannel chanel_parent:channel_parents)
            {
                if (chanel_parent != null)
                {
                    tmp_channel = new Channel();
                    tmp_channel.setId(chanel_parent.getChannelPk());
                    tmp_channel.setName(chanel_parent.getChannelName());
                    tmp_channel.setChannel_parent(chanel_parent.getChannelParent());
                    tmp_channel.setChannel_catalog(chanel_parent.getChannelCatalog());
                    tmp_channel.setChannel_english(chanel_parent.getChannelEnglish());
                    tmp_channel.setChannel_index_count(chanel_parent.getChannelIndexCount());
                    tmp_channel.setChannel_index_name(chanel_parent.getChannelIndexName());
                    tmp_channel.setChannel_index_template(chanel_parent.getChannelIndexTemplate());
                    tmp_channel.setChannel_list_count(chanel_parent.getChannelListCount());
                    tmp_channel.setChannel_list_name(chanel_parent.getChannelListName());
                    tmp_channel.setChannel_list_template(chanel_parent.getChannelListTemplate());
                    tmp_channel.setChannel_page_name(chanel_parent.getChannelPageName());
                    tmp_channel.setChannel_page_template(chanel_parent.getChannelPageTemplate());
                    if (tmp_channel.getChannel_parent().equals("0"))
                    {
                        //递归调用之前先添加到全局list
                        root.add(tmp_channel);
                        //递归调用
                        this.tree(tmp_channel);
                    }
                    else
                    {
                        channel_childs.add(tmp_channel);
                        //递归调用
                        this.tree(tmp_channel);
                    }

                }
            }

        }
        channel.setChildren(channel_childs);
        return root;
    }


    /**
     * 文章列表左侧树形菜单
     */
    public void article_ztree_list()
    {
        List<CmsChannel> List = channelService.findList(new CmsChannel());
        Ztree ztree = null;
        List<Ztree>  ztreelist = new ArrayList<>();
        for (CmsChannel channel1:List)
        {
            ztree = new Ztree();
            ztree.setId(channel1.getChannelPk().toString());
            ztree.setName(channel1.getChannelName());
            ztree.setpId(channel1.getChannelParent());
            ztreelist.add(ztree);
        }
        renderJson(JFinalJson.getJson().toJson(ztreelist));
    }
}
