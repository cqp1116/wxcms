package com.weixun.utils.time;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    /**
     * 得到现在时间
     *
     * @return 字符串 yyyyMMdd
     */
    public static String getStringToday() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    /**
     * 获取当前日期的前一天
     * @return
     */
    public static String getNextDay() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date=new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();
        String dateString = formatter.format(date);
        return dateString;
    }


    public static String getStringYear() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    /**
     * 获取现在时间
     *
     * @return返回字符串格式 yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        return dateString;
    }

    /**
     * 格式化时间，去掉时分秒
     * @param datetime
     * @return
     */
    public static String getStringDate(String datetime) {

        //如果日期不为空则截取，否则赋值为今天的日期
        String res = "";
        if (datetime != null && !datetime.equals(""))
        {
            res = datetime.substring(0,10);
        }
        else
        {
            Date currentTime = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                res = formatter.format(currentTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return  res;
    }
}
